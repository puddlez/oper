﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Oper
{
    public static class Program
    {
        /// <summary>
        /// Title of calling assembly
        /// </summary>
        private static string AssemblyTitle { get; } = (Assembly.GetCallingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute)).SingleOrDefault() as AssemblyTitleAttribute).Title;

        /// <summary>
        /// File version of calling assembly
        /// </summary>
        private static string AssemblyFileVersion { get; } = FileVersionInfo.GetVersionInfo(Assembly.GetCallingAssembly().Location).FileVersion;

        /// <summary>
        /// Title of calling assembly with file version
        /// </summary>
        private static string AssemblyTitleWithVersion => AssemblyTitle + " v" + AssemblyFileVersion.Substring(0, AssemblyFileVersion.IndexOf(".", AssemblyFileVersion.IndexOf(".") + 1));

        public static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(AssemblyTitleWithVersion);
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            if ((args.Length == 0)
                || (args[0].Trim().ToLower() == "help")
                || (args[0].Trim() == "?"))
            {
                Console.WriteLine();
                Console.WriteLine("Usage:\tread targetDirectory [inventoryPath]");
                Console.WriteLine("\texec inventoryPath [logFilePath]");
            }
            else
            {
                var _args = args.ToList();
                {
                    while (_args.Count < 3)
                        _args.Add(null);
                }

                try
                {
                    if (_args[0].ToLower() == "read")
                        Read(_args[1], _args[2]);
                    else if (_args[0].ToLower() == "exec")
                        Execute(_args[1], _args[2]);
                    else
                        throw new ArgumentException("Invalid command: " + args[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.GetType().Name + ": " + ex.Message);
                }
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        private static void Read(string directory, string inventoryPath)
        {
            if (directory == null)
                throw new ArgumentException("Target directory is not defined");

            if (inventoryPath == null)
                inventoryPath = Path.GetDirectoryName(directory + '\\') + "\\operinv.txt";

            Console.WriteLine();
            Console.WriteLine("Reading target directory:");
            Console.WriteLine(directory);

            var filePaths = Directory.GetFiles(directory, "*", SearchOption.AllDirectories)
                .Where(x => (x.Trim().ToLower() != inventoryPath.Trim().ToLower())
                && (x.Trim().ToLower() != inventoryPath.Trim().ToLower() + ".bak")
                && (x.Trim().ToLower() != Path.ChangeExtension(inventoryPath, ".log.txt").Trim().ToLower())).ToArray();

            Console.WriteLine();
            Console.WriteLine("Writing inventory file:");
            Console.WriteLine(inventoryPath);

            ValidateDirectory(inventoryPath);

            using (var file = new StreamWriter(inventoryPath))
            {
                file.WriteLine("target directory:");
                file.WriteLine(directory);
                file.WriteLine("file paths (modify below):");
                file.Write(string.Join("\r\n", filePaths));
            }
        }

        private static void Execute(string inventoryPath, string logFilePath)
        {
            if (inventoryPath == null)
                throw new ArgumentException("Inventory file path is not defined");

            if (logFilePath == null)
                logFilePath = Path.ChangeExtension(inventoryPath, ".log.txt");

            Console.WriteLine();
            Console.WriteLine("Reading inventory file:");
            Console.WriteLine(inventoryPath);

            string directory = null;
            var modifiedFilePaths = new List<string>();
            {
                using (var fileRead = new FileStream(inventoryPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) using (var file = new StreamReader(fileRead))
                {
                    if (file.ReadLine().ToLower().StartsWith("target directory") == false)
                        throw new FileLoadException("Key expected: \"target directory\"");

                    directory = file.ReadLine();

                    if (file.ReadLine().ToLower().StartsWith("file paths") == false)
                        throw new FileLoadException("Key expected: \"file paths\"");

                    {
                        var text = file.ReadToEnd().Replace("\r\n", "\n");
                        var modifiedFilePath = "";

                        for (var i = 0; i < text.Length; i++)
                        {
                            if (text[i] == '\n')
                            {
                                modifiedFilePaths.Add(modifiedFilePath);
                                modifiedFilePath = "";
                            }                                
                            else
                                modifiedFilePath += text[i];
                        }

                        modifiedFilePaths.Add(modifiedFilePath);
                    }
                }

                modifiedFilePaths = modifiedFilePaths.Select(x => x.Trim()).ToList();
            }

            Console.WriteLine();
            Console.WriteLine("Starting log file:");
            Console.WriteLine(logFilePath);

            ValidateDirectory(logFilePath);

            using (var log = new StreamWriter(logFilePath))
            {
                log.WriteLine(AssemblyTitleWithVersion);
                log.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

                log.WriteLine();
                log.WriteLine("Using inventory file:");
                log.WriteLine(inventoryPath);
                log.ConsoleWriteLine();
                log.ConsoleWriteLine("Reading target directory:");
                log.ConsoleWriteLine(directory);
                
                var existingFilePaths = Directory.GetFiles(directory, "*", SearchOption.AllDirectories)
                    .Where(x => (x.Trim().ToLower() != inventoryPath.Trim().ToLower())
                    && (x.Trim().ToLower() != inventoryPath.Trim().ToLower() + ".bak")
                    && (x.Trim().ToLower() != logFilePath.Trim().ToLower())).ToList();

                if (existingFilePaths.Count != modifiedFilePaths.Count)
                {
                    log.WriteLine();
                    log.WriteLine("Count of file paths in inventory file does not match target directory.");
                    log.WriteLine("File paths in target directory: " + existingFilePaths.Count);
                    log.WriteLine("File paths in inventory file:   " + modifiedFilePaths.Count);

                    for (var i = 0; (i < existingFilePaths.Count) || (i < modifiedFilePaths.Count); i++)
                    {
                        log.WriteLine();

                        if (i < existingFilePaths.Count)
                            log.WriteLine("Existing: " + existingFilePaths[i]);

                        if (i < modifiedFilePaths.Count)
                            log.WriteLine("Modified: " + modifiedFilePaths[i]);
                    }

                    throw new ArgumentOutOfRangeException("", "Count of file paths in inventory file does not match target directory.  See log.");
                }                        

                for (var i = 0; i < existingFilePaths.Count; i++)
                    if (existingFilePaths[i].Trim().ToLower() == modifiedFilePaths[i].Trim().ToLower())
                    {
                        existingFilePaths.RemoveAt(i);
                        modifiedFilePaths.RemoveAt(i);
                        i--;
                    }

                if (modifiedFilePaths.Count == 0)
                {
                    log.ConsoleWriteLine(); 
                    log.ConsoleWriteLine("No changes required.");
                }                    
                else
                {
                    log.ConsoleWriteLine();
                    log.ConsoleWriteLine("Executing changes...");

                    var directories = new HashSet<string>();
                    var exceptions = 0;

                    for (var i = 0; i < modifiedFilePaths.Count; i++)
                    {
                        try
                        {
                            var file = new FileInfo(existingFilePaths[i]);
                            file.IsReadOnly = false;

                            if (string.IsNullOrWhiteSpace(modifiedFilePaths[i]))
                            {
                                log.ConsoleWriteLine();
                                    log.ConsoleWriteLine("Recycling " + existingFilePaths[i]);

                                Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(
                                    existingFilePaths[i],
                                    Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                                    Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);

                                directories.Add(Path.GetDirectoryName(existingFilePaths[i]));
                            }
                            else
                            {
                                if (Path.GetDirectoryName(existingFilePaths[i]).Trim().ToLower()
                                    == Path.GetDirectoryName(modifiedFilePaths[i]).Trim().ToLower())
                                {
                                    log.ConsoleWriteLine();
                                    log.ConsoleWriteLine("Renaming  " + existingFilePaths[i]);
                                    log.ConsoleWriteLine("      to  " + modifiedFilePaths[i]);
                                }
                                else
                                {
                                    log.ConsoleWriteLine();
                                    log.ConsoleWriteLine("Moving    " + existingFilePaths[i]);
                                    log.ConsoleWriteLine("    to    " + modifiedFilePaths[i]);

                                    ValidateDirectory(modifiedFilePaths[i]);

                                    directories.Add(Path.GetDirectoryName(existingFilePaths[i]));
                                }

                                file.MoveTo(modifiedFilePaths[i]);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.ConsoleWriteLine(ex.GetType().Name + ": " + ex.Message);
                            exceptions++;
                        }
                    }

                    {
                        var emptyDirectories = directories.Where(x => (Directory.GetDirectories(x).Length == 0)
                            && (Directory.GetFiles(x).Length == 0)).ToArray();

                        if (emptyDirectories.Any())
                        {
                            log.ConsoleWriteLine();

                            for (var i = 0; i < emptyDirectories.Length; i++)
                            {
                                log.ConsoleWriteLine("Recycling empty directory " + emptyDirectories[i]);

                                try
                                {
                                    Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(
                                        emptyDirectories[i],
                                        Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                                        Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin);
                                }
                                catch (Exception ex)
                                {
                                    log.ConsoleWriteLine(ex.GetType().Name + ": " + ex.Message);
                                    exceptions++;
                                }
                            }
                        }
                    }

                    log.ConsoleWriteLine();
                    log.ConsoleWriteLine(exceptions + " exceptions encountered.");
                }
            }
        }

        private static void ValidateDirectory(string path)
        {
            if (Directory.Exists(Path.GetDirectoryName(path)) == false)
            {
                try
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                }
                catch
                {
                    throw new DirectoryNotFoundException("Unable to access or create directory:\r\n" + Path.GetDirectoryName(path));
                }
            }
        }

        private static void ConsoleWrite(this StreamWriter log, string text = "")
        {
            Console.Write(text);
            log.Write(text);
        }

        private static void ConsoleWriteLine(this StreamWriter log, string text = "")
        {
            Console.WriteLine(text);
            log.WriteLine(text);
        }
    }
}