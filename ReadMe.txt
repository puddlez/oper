-Oper-
Written by Robert Lloyd
robert.lloyd@agilent.com

A simple command line utility for mass execution of simple file operations.

-

Usage:

	read targetDirectory [inventoryPath]

		reads targetDirectory and writes inventory file to inventoryPath
		
		if inventoryPath is omitted, writes to targetDirectory\operinv.txt

	exec inventoryPath [logFilePath]

		executes inventory file at inventoryPath and writes log to logFilePath

		if logFilePath is omitted, writes to inventoryPath.log.txt

Editing Inventory File:

	Edit file paths to rename or move files

	Remove file paths to delete files (must leave blank line)

	Execution will fail if count of file paths in inventory file (including blank lines) does not match target directory